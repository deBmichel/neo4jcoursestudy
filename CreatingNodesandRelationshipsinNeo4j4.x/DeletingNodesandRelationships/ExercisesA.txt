EXERCISE 11.1: DELETE A RELATIONSHIP (INSTRUCTIONS):

	Recall that in the graph we have been working with, we have the HELPED relationship 
	between Tom Hanks and Gary Sinise. We have decided that we no longer need this 
	relationship in the graph.

	Delete the HELPED relationship from the graph.

	// My solution 
		MATCH () -[rel:HELPED]-()
		DELETE rel

		//UPS here I was very crazy : OR THE PHRase 
		// Delete the HELPED relationship from the graph
		Is not very clear.

	// The solution 
		MATCH (:Person)-[rel:HELPED]-(:Person)
		DELETE rel

EXERCISE 11.2: CONFIRM THAT THE RELATIONSHIP HAS BEEN DELETED (INSTRUCTIONS):

	Query the graph to confirm that the relationship no longer exists

	// My solution 
		MATCH () -[rel:HELPED]-()
		RETURN rel

	// The solution 
		MATCH (:Person)-[rel:HELPED]-(:Person)
		RETURN rel

EXERCISE 11.3: RETRIEVE A MOVIE AND ALL OF ITS RELATIONSHIPS (INSTRUCTIONS):

	Query the graph to display Forrest Gump and all of its relationships.

	// My solution
		MATCH (n)-[rel]-(m:Movie {title:"Forrest Gump"})
		RETURN n, rel, m

		// I prefer my solution, because is : all of its relationships
		// NOT only the Person relation to the movie Forrest Gump is
		// a situation of ideas.
	
	// The solution
		MATCH (p:Person)-[rel]-(m:Movie)
		WHERE m.title = 'Forrest Gump'
		RETURN p, rel, m


EXERCISE 11.4: TRY DELETING A NODE WITHOUT DETACHING ITS RELATIONSHIPS (INSTRUCTIONS):

	We want to remove the movie, Forrest Gump from the graph.

	Try deleting the Forrest Gump node without detaching its relationships.

	Do you receive an error?

	// My solution
		MATCH (m:Movie)
		WHERE m.title = "Forrest Gump"
		DELETE m

		the expected error:

			Neo.ClientError.Schema.ConstraintValidationFailed
			Cannot delete node<171>, because it still has relationships. To delete this node, you 
			must first delete its relationships.

	// The solution:
		MATCH (m:Movie)
		WHERE m.title = 'Forrest Gump'
		DELETE m

EXERCISE 11.5: DELETE A MOVIE NODE, ALONG WITH ITS RELATIONSHIPS (INSTRUCTIONS):

	Delete Forrest Gump, along with its relationships in the graph.

	// My solution
		MATCH (m:Movie)
		WHERE m.title = "Forrest Gump"
		DETACH DELETE m

	// The solution
		MATCH (m:Movie)
		WHERE m.title = 'Forrest Gump'
		DETACH DELETE m

EXERCISE 11.6: CONFIRM THAT THE MOVIE NODE HAS BEEN DELETED (INSTRUCTIONS):

	Query the graph to confirm that the Forrest Gump node has been deleted.

	// My solution
		MATCH (n)-[rel]-(m:Movie {title:"Forrest Gump"})
		RETURN n, rel, m

	// The solution
		MATCH (p:Person)-[rel]-(m:Movie)
		WHERE m.title = 'Forrest Gump'
		RETURN p, rel, m
