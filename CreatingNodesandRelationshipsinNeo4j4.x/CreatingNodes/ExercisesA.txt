EXERCISE 9.1: CREATE A MOVIE NODE (INSTRUCTIONS):

	Create a Movie node for the movie with the title, Forrest Gump.

	//My solution:
	CREATE (m:Movie {title:"Forrest Gump"})

	//The solution:
	CREATE (:Movie {title: 'Forrest Gump'})


EXERCISE 9.2: RETRIEVE THE NEWLY-CREATED NODE (INSTRUCTIONS)

	Retrieve the node you just created by its title.

	//My solution:
		MATCH (m:Movie) 
		WHERE m.title = "Forrest Gump"
		RETURN m

	//The solution:
		MATCH (m:Movie)
		WHERE m.title = 'Forrest Gump'
		RETURN m


EXERCISE 9.3: CREATE A PERSON NODE (INSTRUCTIONS):

	Create a Person node for the person with the name, Robin Wright.

	//My solution:
	CREATE (p:Person {name:"Robin Wright"})

	//The solution:
	CREATE (:Person {name: 'Robin Wright'})
	

EXERCISE 9.4: RETRIEVE THE PERSON NODE YOU JUST CREATED BY ITS NAME (INSTRUCTIONS)

	Retrieve the Person node you just created by its name.

	//My solution:
		MATCH (p:Person) 
		WHERE p.name = "Robin Wright"
		RETURN p

	//The solution:
		MATCH (p:Person)
		WHERE p.name = 'Robin Wright'
		RETURN p


EXERCISE 9.5: ADD A LABEL TO A NODE (INSTRUCTIONS)
	
	Add the label OlderMovie to any Movie node that was released before 2010:

	//My solution:
		MATCH (m:Movie)
		WITH m AS M LIMIT 1
		WHERE M.released < 2010
		SET M:OlderMovie
		RETURN DISTINCT labels(M)

	//The solution:
		MATCH (m:Movie)
		WHERE m.released < 2010
		SET m:OlderMovie
		RETURN DISTINCT labels(m)

EXERCISE 9.6: RETRIEVE THE NODE USING THE NEW LABEL (INSTRUCTIONS)

	Retrieve all older movie nodes to test that the label was indeed added to these nodes.

	//My solution:
		MATCH (m:OlderMovie)
		RETURN m

	//The solution:	
		MATCH (m:OlderMovie)
		RETURN m.title, m.released

EXERCISE 9.7: ADD THE FEMALE LABEL TO SELECTED NODES (INSTRUCTIONS)

	Add the label Female to all Person nodes that has a person whose name starts with Robin.

	//My solution:
		MATCH (p:Person)
		WHERE p.name STARTS WITH 'Robin'
		SET p:Female
		RETURN p

	//The solution:
		MATCH (p:Person)
		WHERE p.name STARTS WITH 'Robin'
		SET p:Female

EXERCISE 9.8: RETRIEVE ALL FEMALE NODES (INSTRUCTIONS)

	Retrieve all Female nodes:

	//My solution:
		MATCH (p:Female)
		RETURN p

	//The solution:
		MATCH (p:Female)
		RETURN p.name

EXERCISE 9.9: REMOVE THE FEMALE LABEL FROM THE NODES THAT HAVE THIS LABEL (INSTRUCTIONS)

	We’ve decided to not use the Female label. Remove the Female label from the nodes that have this label.

	//My solution:
		MATCH (p:Female)
		REMOVE p:Female
		RETURN p

	// The solution:
		MATCH (p:Female)
		REMOVE p:Female

EXERCISE 9.10: VIEW THE CURRENT SCHEMA OF THE GRAPH (INSTRUCTIONS):

	View the current schema of the graph.

	CALL db.schema.visualization(); // It is a basic command to learn.

EXERCISE 9.11: ADD PROPERTIES TO A MOVIE (INSTRUCTIONS):

	Add the following properties to the movie, Forrest Gump:

		released: 1994
		tagline: Life is like a box of chocolates…​you never know what you’re gonna get.
		lengthInMinutes: 142

	-> Hint: This movie will also have the label OlderMovie.

	// My solution
		MATCH (m:Movie) 
		WHERE m.title = "Forrest Gump"
		SET m:OlderMovie, m += {
			released: "1994",
			tagline: "Life is like a box of chocolates…​you never know what you’re gonna get",
			lengthInMinutes: "142",
			Hint: "This movie will also have the label OlderMovie."
		}
		RETURN m

	// The solution
		MATCH (m:Movie)
		WHERE m.title = 'Forrest Gump'
		SET m:OlderMovie,
		    m.released = 1994,
		    m.tagline = "Life is like a box of chocolates...you never know what you're gonna get.",
		    m.lengthInMinutes = 142

EXERCISE 9.12: RETRIEVE AN OLDERMOVIE NODE TO CONFIRM THE LABEL AND PROPERTIES (INSTRUCTIONS):

	Retrieve this OlderMovie node to confirm that the properties and label have been 
	properly set.

	// My solution
		MATCH (m:OlderMovie) RETURN m // It is easy and older movies come with the Forrest Movie.

	// The solution
		MATCH (m:OlderMovie)
		WHERE m.title = 'Forrest Gump'
		RETURN m

EXERCISE 9.13: ADD PROPERTIES TO THE PERSON, ROBIN WRIGHT (INSTRUCTIONS):

	Add the following properties to the person, Robin Wright:

		born: 1966
		birthPlace: Dallas

	// My solution
		MATCH (p:Person) 
		WHERE p.name = "Robin Wright"
		SET p += {
			born: "1966",
			birthPlace: "Dallas"
		}
		RETURN p

	// I preffer use += { } Because in the notes of the course says it is more elastic-

	// The solution
		MATCH (p:Person)
		WHERE p.name = 'Robin Wright'
		SET p.born = 1966, p.birthPlace = 'Dallas'

EXERCISE 9.14: RETRIEVE AN UPDATED PERSON NODE (INSTRUCTIONS):

	Retrieve this Person node to confirm that the properties have been properly set.
	
	// My solution
		MATCH (p:Person) 
		WHERE p.name = "Robin Wright"
		RETURN p

	// The solution
		MATCH (p:Person)
		WHERE p.name = 'Robin Wright'
		RETURN p

EXERCISE 9.15: REMOVE A PROPERTY FROM A MOVIE NODE (INSTRUCTIONS):

	Next, you will remove properties from specific nodes in the graph.

	Remove the lengthInMinutes property from the movie, Forrest Gump.

	// My solution
		MATCH (m:Movie)
		WHERE m.title = "Forrest Gump"
		REMOVE m.lengthInMinutes
		RETURN m

		or SET m.lengthInMinutes = NULL
	
	// The solution
		MATCH (m:Movie)
		WHERE m.title = 'Forrest Gump'  // ERROR ON THE PLATFORM SHOWING THE CODE.
		SET m.lengthInMinutes = null

EXERCISE 9.16: RETRIEVE THE NODE TO CONFIRM THAT THE PROPERTY HAS BEEN REMOVED (INSTRUCTIONS):

	Retrieve the Forrest Gump node to confirm that the property has been removed:

	// My solution
		MATCH (m:Movie)
		WHERE m.title = 'Forrest Gump'
		RETURN m

	// The solution
		MATCH (m:Movie)
		WHERE m.title = 'Forrest Gump'
		RETURN m

EXERCISE 9.17: REMOVE A PROPERTY FROM A PERSON NODE (INSTRUCTIONS):

	Remove the birthPlace property from the person, Robin Wright.

	// My solution
		MATCH (p:Person) 
		WHERE p.name = "Robin Wright"
		SET p.birthPlace = null

	// The solution
		MATCH (p:Person)
		WHERE p.name = 'Robin Wright'
		REMOVE p.birthPlace

EXERCISE 9.18: RETRIEVE THE NODE TO CONFIRM THAT THE PROPERTY HAS BEEN REMOVED (INSTRUCTIONS):

	Retrieve the Robin Wright node to confirm that the property has been removed.

	// My solution
		MATCH (p:Person) 
		WHERE p.name = "Robin Wright"
		RETURN p

	// The solution
		MATCH (p:Person)
		WHERE p.name = 'Robin Wright'
		RETURN p





