EXERCISE 10.1: CREATE ACTED_IN RELATIONSHIPS (INSTRUCTIONS):

	In the last exercise, you created the node for the movie, Forrest Gump and the person, 
	Robin Wright.

	Create the ACTED_IN relationship between the actors, Robin Wright, Tom Hanks, and Gary 
	Sinise and the movie, Forrest Gump.

	// My Solution
		MATCH (m:Movie), (a:Person), (b:Person), (c:Person)
		WHERE 
			m.title = 'Forrest Gump' AND
			a.name = "Robin Wright" AND
			b.name = "Tom Hanks" AND
			c.name = "Gary Sinise" 
			AND NOT exists((a)-[:ACTED_IN]->(m))
			AND NOT exists((b)-[:ACTED_IN]->(m))
			AND NOT exists((c)-[:ACTED_IN]->(m))
		CREATE 
			(a)-[:ACTED_IN]->(m), (b)-[:ACTED_IN]->(m), (c)-[:ACTED_IN]->(m)
		RETURN a, b, c, m

	// The Solution
		MATCH (m:Movie)
		WHERE m.title = 'Forrest Gump'
		MATCH (p:Person)
		WHERE p.name = 'Tom Hanks' OR p.name = 'Robin Wright' OR p.name = 'Gary Sinise'
		CREATE (p)-[:ACTED_IN]->(m)

EXERCISE 10.2: CREATE A DIRECTED RELATIONSHIP (INSTRUCTIONS):

	Create the DIRECTED relationship between Robert Zemeckis and the movie, Forrest Gump.

	// My Solution
	CREATE (p:Person{name:"Robert Zemeckis"})-[:DIRECTED]->(m:Movie{name:"Forrest Gump"})
	RETURN p, rel, m

	// The Solution
		MATCH (m:Movie)
		WHERE m.title = 'Forrest Gump'
		MATCH (p:Person)
		WHERE p.name = 'Robert Zemeckis'
		CREATE (p)-[:DIRECTED]->(m)

	I preffer my solution.

EXERCISE 10.3: CREATE A HELPED RELATIONSHIP (INSTRUCTIONS):

	Create a new relationship, HELPED from Tom Hanks to Gary Sinise.

	// My Solution
		CREATE (pf:Person{name:"Tom Hanks"}) -[rel:HELPED]-> (pt:Person{name:"Gary Sinise"})
		RETURN pf, rel, pt

	// The Solution
		MATCH (p1:Person)
		WHERE p1.name = 'Tom Hanks'
		MATCH (p2:Person)
		WHERE p2.name = 'Gary Sinise'
		CREATE (p1)-[:HELPED]->(p2)

	I preffer my solution. DOUBLE MATCH IS SLOW, SURE. 3:) 

EXERCISE 10.4: QUERY NODES AND NEW RELATIONSHIPS (INSTRUCTIONS)

	Write a Cypher query to return all nodes connected to the movie, Forrest Gump, along with 
	their relationships.

	// My Solution
		MATCH (p) -[rel]- (m:Movie {title:"Forrest Gump"})
		RETURN p, rel, m

	// The Solution
		MATCH (p:Person)-[rel]-(m:Movie)
		WHERE m.title = 'Forrest Gump'
		RETURN p, rel, m

EXERCISE 10.5: ADD PROPERTIES TO RELATIONSHIPS (INSTRUCTIONS)

	Next, you will add some properties to the relationships that you just created.

	Add the roles property to the three ACTED_IN relationships that you just created to the 
	movie, Forrest Gump using this information: Tom Hanks played the role, Forrest Gump. Robin 
	Wright played the role, Jenny Curran. Gary Sinise played the role, Lieutenant Dan Taylor.

	Hint: You can set each relationship using separate MATCH clauses. You can also use a CASE 
	clause to set the values. Look up in the documentation for how to use the CASE clause.

	MATCH (p:Person)-[rel:ACTED_IN]->(m:Movie)
	WHERE m.title = 'Forrest Gump'
	SET rel.roles =
	CASE p.name
		WHEN 'Tom Hanks' THEN ['Forrest Gump']
		WHEN 'Robin Wright' THEN ['Jenny Curran']
		WHEN 'Gary Sinise' THEN ['Lieutenant Dan Taylor']
	END

EXERCISE 10.6: ADD A PROPERTY TO THE HELPED RELATIONSHIP (INSTRUCTIONS)

	Add a new property, research to the HELPED relationship between Tom Hanks and Gary Sinise 
	and set this property’s value to war history.
	// My Solution
		MATCH (p:Person) -[rel:HELPED]-> (pf:Person)
		WHERE p.name = "Tom Hanks" AND pf.name = "Gary Sinise"
		SET rel.research = "war history"
		RETURN p, rel, pf

	// The Solution
		MATCH (p1:Person)-[rel:HELPED]->(p2:Person)
		WHERE p1.name = 'Tom Hanks' AND p2.name = 'Gary Sinise'
		SET rel.research = 'war history'

EXERCISE 10.7: VIEW THE CURRENT LIST OF PROPERTY KEYS IN THE GRAPH (INSTRUCTIONS):

	View the current list of property keys in the graph.

	BASIC COMMAND : CALL db.propertyKeys()

EXERCISE 10.8: VIEW THE CURRENT SCHEMA OF THE GRAPH (INSTRUCTIONS)

	View the current schema of the graph.

	BASIC COMMAND : CALL db.schema.visualization()

EXERCISE 10.9: RETRIEVE THE NAMES AND ROLES FOR ACTORS (INSTRUCTIONS)

	Query the graph to return the names and roles of actors in the movie, Forrest Gump.

	// My Solution
		MATCH (p:Person) -[rel:ACTED_IN]-> (m:Movie {title:"Forrest Gump"})
		RETURN p, rel.roles

	// The Solution
		MATCH (p:Person)-[rel:ACTED_IN]->(m:Movie)
		WHERE m.title = 'Forrest Gump'
		RETURN p.name, rel.roles

EXERCISE 10.10: RETRIEVE INFORMATION ABOUT ANY SPECIFIC RELATIONSHIPS (INSTRUCTIONS)

	Query the graph to retrieve information about any HELPED relationships.	

	// EQUALITY IN SOLUTIONS OMG !!!
		MATCH (p1:Person)-[rel:HELPED]-(p2:Person)
		RETURN p1.name, rel, p2.name

EXERCISE 10.11: MODIFY A PROPERTY OF A RELATIONSHIP (INSTRUCTIONS)

	Next, you will modify existing properties for a relationship and also remove them.

	Modify the role that Gary Sinise played in the movie, Forrest Gump from Lieutenant 
	Dan Taylor to Lt. Dan Taylor.

	// My Solution
		MATCH (p:Person) -[rel:ACTED_IN]-> (m:Movie)
		WHERE m.title = 'Forrest Gump' AND p.name = "Gary Sinise"
		SET rel.roles = ["Lt. Dan Taylor"]
		RETURN p, rel, m

	// The Solution
		MATCH (p:Person)-[rel:ACTED_IN]->(m:Movie)
		WHERE m.title = 'Forrest Gump' AND p.name = 'Gary Sinise'
		SET rel.roles =['Lt. Dan Taylor']

EXERCISE 10.12: REMOVE A PROPERTY FROM A RELATIONSHIP (INSTRUCTIONS)

	Remove the research property from the HELPED relationship from Tom Hanks to Gary Sinise.

	// My Solution
		MATCH (p1:Person)-[rel:HELPED]->(p2:Person)
		WHERE p1.name = 'Tom Hanks' AND p2.name = 'Gary Sinise'
		REMOVE rel.research
		RETURN p1, p2, rel

	// The Solution
		MATCH (p1:Person)-[rel:HELPED]->(p2:Person)
		WHERE p1.name = 'Tom Hanks' AND p2.name = 'Gary Sinise'
		REMOVE rel.research

EXERCISE 10.13: CONFIRM THAT YOUR MODIFICATIONS WERE MADE TO THE GRAPH (INSTRUCTIONS)

	Query the graph to confirm that your modifications were made to the graph.

	// The ideas here were a lot, I can use two or three or four commands here.

	MATCH (p:Person)-[rel:ACTED_IN]->(m:Movie)
	WHERE m.title = 'Forrest Gump'
	return p, rel, m



