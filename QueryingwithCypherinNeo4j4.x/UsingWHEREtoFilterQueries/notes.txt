TESTING EQUALITY:

	You have learned how to specify values for properties of nodes and relationships 
	to filter what data is returned from the MATCH and RETURN clauses. The format for 
	filtering you have learned thus far only tests equality, where you must specify 
	values for the properties to test with. What if you wanted more flexibility about 
	how the query is filtered? For example, you want to retrieve all movies released 
	after 2000, or retrieve all actors born after 1970 who acted in movies released 
	before 1995. Most applications need more flexibility in how data is filtered.

	The most common clause you use to filter queries is the WHERE clause that typically
	follows a MATCH clause. In the WHERE clause, you can place conditions that are 
	evaluated at runtime to filter the query.

	Previously, you learned to write simple query as follows:

		MATCH (p:Person)-[:ACTED_IN]->(m:Movie {released: 2008})
		RETURN p, m

		Here is a way you specify the same query using the WHERE clause:

		MATCH (p:Person)-[:ACTED_IN]->(m:Movie)
		WHERE m.released = 2008
		RETURN p, m


TESTING MULTIPLE VALUES:

	In this example, you can only refer to named nodes or relationships in a WHERE 
	clause so remember that you must specify a variable for any node or relationship 
	you are testing in the WHERE clause. The benefit of using a WHERE clause is that 
	you can specify potentially complex conditions for the query.

		MATCH (p:Person)-[:ACTED_IN]->(m:Movie)
		WHERE m.released = 2008 OR m.released = 2009
		RETURN p, m


EXAMPLE: SPECIFYING A RANGE DIFFERENTLY:

	You can also specify the same query as:

	MATCH (p:Person)-[:ACTED_IN]->(m:Movie)
	WHERE 2003 <= m.released <= 2004
	RETURN p.name, m.title, m.released

	You can specify conditions in a WHERE clause that return a value of true or false 
	(for example predicates). For testing numeric values, you use the standard numeric 
	comparison operators. Each condition can be combined for runtime evaluation using 
	the boolean operators AND, OR, XOR, and NOT. There are a number of numeric functions 
	you can use in your conditions. See the Neo4j Cypher Manual’s section Mathematical 
	Functions for more information.

	A special condition in a query is when the retrieval returns an unknown value called 
	null. You should read the Neo4j Cypher Manual’s section Working with null to 
	understand how null values are used at runtime.


TESTING LABELS:
	Thus far, you have used the node labels to filter queries in a MATCH clause. You can 
	filter node labels in the WHERE clause also:

	For example, these two Cypher queries:

	MATCH (p:Person)
	RETURN p.name
	
	MATCH (p:Person)-[:ACTED_IN]->(:Movie {title: 'The Matrix'})
	RETURN p.name
	
	can be rewritten using WHERE clauses as follows:

	MATCH (p)
	WHERE p:Person
	RETURN p.name
	
	MATCH (p)-[:ACTED_IN]->(m)
	WHERE p:Person AND m:Movie AND m.title='The Matrix'
	RETURN p.name
	
	Here is the result of running this second query: Not all node labels need to be tested 
	during a query. If your graph has multiple labels for the same node, filtering it by 
	the node label will provide better query performance.


TESTING EXISTENCE OF A PROPERTY:

	Recall that a property is associated with a particular node or relationship. A 
	property is not associated with a node with a particular label or relationship 
	type. In one of our queries earlier, we saw that the movie "Something’s Gotta 
	Give" is the only movie in the Movie database that does not have a tagline 
	property. Suppose we only want to return the movies that the actor, Jack Nicholson 
	acted in with the condition that they must all have a tagline.

	Here is the query to retrieve the specified movies where we test the existence of 
	the tagline property:

	MATCH (p:Person)-[:ACTED_IN]->(m:Movie)
	WHERE p.name='Jack Nicholson' AND exists(m.tagline)
	RETURN m.title, m.tagline


TESTING STRINGS:

	Cypher has a set of string-related keywords that you can use in your WHERE clauses 
	to test string property values. You can specify STARTS WITH, ENDS WITH, and 
	CONTAINS.

	For example, to find all actors in the Movie database whose first name is Michael, 
	you would write:

	MATCH (p:Person)-[:ACTED_IN]->()
	WHERE p.name STARTS WITH 'Michael'
	RETURN p.name

	I can writte this like:

		MATCH (p:Person)
		WHERE p.name STARTS WITH 'Michael'
		RETURN p.name


STRING COMPARISONS ARE CASE-SENSITIVE:

	Note that the comparison of strings is case-sensitive. There are a number of 
	string-related functions you can use to further test strings. For example, if 
	you want to test a value, regardless of its case, you could call the toLower() 
	function to convert the string to lower case before it is compared.

	MATCH (p:Person)-[:ACTED_IN]->()
	WHERE toLower(p.name) STARTS WITH 'michael'
	RETURN p.name
	
	In this example where we are converting a property to lower case, if an index 
	has been created for this property, it will not be used at runtime.
	See the String functions section of the Neo4j Cypher Manual for more information. 
	It is sometimes useful to use the built-in string functions to modify the data 
	that is returned in the query in the RETURN clause.


TESTING WITH REGULAR EXPRESSIONS:

	If you prefer, you can test property values using regular expressions. You use 
	the syntax =~ to specify the regular expression you are testing with. Here is 
	an example where we test the name of the Person using a regular expression to 
	retrieve all Person nodes with a name property that begins with 'Tom':

	MATCH (p:Person)
	WHERE p.name =~'Tom.*'
	RETURN p.name

	If you specify a regular expression. The index will never be used. In addition, 
	the property value must fully match the regular expression.


EXAMPLE: TESTING WITH PATTERNS - 1:
	Sometimes during a query, you may want to perform additional filtering using 
	the relationships between nodes being visited during the query. For example, 
	during retrieval, you may want to exclude certain paths traversed. You can 
	specify a NOT specifier on a pattern in a WHERE clause.

	Here is an example where we want to return all Person nodes of people who 
	wrote movies:

	MATCH (p:Person)-[:WROTE]->(m:Movie)
	RETURN p.name, m.title


EXAMPLE: TESTING WITH PATTERNS - 2:
	Next, we modify this query to exclude people who directed that particular movie:

	MATCH (p:Person)-[:WROTE]->(m:Movie)
	WHERE NOT exists( (p)-[:DIRECTED]->(m) )
	RETURN p.name, m.title


EXAMPLE: TESTING WITH PATTERNS - 3:
	Here is another example where we want to find Gene Hackman and the movies that 
	he acted in with another person who also directed the movie.

	MATCH (gene:Person)-[:ACTED_IN]->(m:Movie)<-[:ACTED_IN]-(other:Person)
	WHERE gene.name= 'Gene Hackman'
	AND exists( (other)-[:DIRECTED]->(m) )
	RETURN  gene, other, m

	tRYING REWRITE THE CypherQL	but here the idea does not work : reason [NOTATION]

	MATCH (gene:Person)-[:ACTED_IN]->(m:Movie)<-[:ACTED_IN | :DIRECTED]-(other:Person)
	WHERE gene.name= 'Gene Hackman'
	RETURN  gene, other, m


Testing with list values
	If you have a set of values you want to test with, you can place them in a list 
	or you can test with an existing list in the graph. A Cypher list is a 
	comma-separated set of values within square brackets.

	You can define the list in the WHERE clause. During the query, the graph engine 
	will compare each property with the values IN the list. You can place either 
	numeric or string values in the list, but typically, elements of the list are 
	of the same type of data. If you are testing with a property of a string type, 
	then all the elements of the list should be strings.

	In this example, we only want to retrieve Person nodes of people born in 1965 or 1970:

	MATCH (p:Person)
	WHERE p.born IN [1965, 1970]
	RETURN p.name as name, p.born as yearBorn


TESTING LIST VALUES IN THE GRAPH:

	You can also compare a value to an existing list in the graph.

	We know that the :ACTED_IN relationship has a property, roles that contains the 
	list of roles an actor had in a particular movie they acted in. Here is the query 
	we write to return the name of the actor who played Neo in the movie The Matrix:

	MATCH (p:Person)-[r:ACTED_IN]->(m:Movie)
	WHERE  'Neo' IN r.roles AND m.title='The Matrix'
	RETURN p.name

In the course Creating Nodes and Relationships in Neo4j 4.x, you will learn how to create 
lists from your queries by aggregating data in the graph.

There are a number of syntax elements of Cypher that we have not covered in this training. 
For example, you can specify CASE logic in your conditional testing for your WHERE clauses. 
You can learn more about these syntax elements in the Neo4j Cypher Manual and the Cypher 
Refcard.





	


