What is Cypher ?

	Cypher is a declarative query language that allows for expressive and efficient 
	querying and updating of graph data. Cypher is a relatively simple and very 
	powerful language. Complex database queries can easily be expressed through Cypher, 
	allowing you to focus on your domain instead of getting lost in the syntax of 
	database access.

	Cypher is designed to be a human-friendly query language, suitable for both 
	developers and other professionals. The guiding goal is to make the simple things 
	easy, and the complex things possible.

IMPORTANT HAVE IN MIND THE cYPHER REFCARD:

	Source: https://neo4j.com/docs/cypher-refcard/current/


Cypher is ASCII art
	Optimized for being read by humans, Cypher’s construct uses English prose and 
	iconography (called ASCII Art) to make queries more self-explanatory.

	Things like : (A)-[:likes]->(B), (A); match (n) return n;

CYPHER EXPRESSES WHAT TO RETRIEVE, NOT HOW TO RETRIEVE IT:
	Being a declarative language, Cypher focuses on the clarity of expressing what 
	to retrieve from a graph, not on how to retrieve it. You can think of Cypher as 
	mapping English language sentence structure to patterns in a graph. For example, 
	the nouns are nodes of the graph, the verbs are the relationships in the graph, 
	and the adjectives and adverbs are the properties.

	This is in contrast to imperative, programmatic APIs for database access. This 
	approach makes query optimization an implementation detail instead of a burden 
	on the developer, removing the requirement to update all traversals just because 
	the physical database structure has changed.

	Cypher is inspired by a number of different approaches and builds upon established 
	practices for expressive querying. Many of the Cypher keywords like WHERE and ORDER 
	BY are inspired by SQL. The pattern matching functionality of Cypher borrows concepts 
	from SPARQL. And some of the collection semantics have been borrowed from languages 
	such as Haskell and Python.

	The Cyphguage has been made available to anyone to implement and use via openCypher 
	(opencypher.org), allowing any database vendor, researcher or other interested party 
	to reap the benefits of our years of effort and experience in developing a first class 
	graph query language.

NODE SINTAX:

	Cypher uses a pair of parentheses like (), (n) to represent a node, much like a circle 
	on a whiteboard.

	Here is the simplified syntax for specifying a node:

	Syntax
	()
	(<variable>)
	Notice that a node MUST have the parentheses.

	When you specify (n) for a node, you are telling the query processor that for this query, 
	use the variable n to represent nodes that will be processed later in the query for further 
	query processing or for returning values from the query. If you do not need to do anything 
	with the node, you can skip the use of the variable. This is called an anonymous node.

	Recall that a node typically represents an entity in your domain.

LABEL SYNTAX:
	Nodes in a graph are typically labeled. Labels are used to group nodes and filter queries 
	against the graph. That is, labels can be used to optimize queries.

	Here are examples for specifying nodes with labels using the the above image:

	Syntax
	(:Person)
	(p:Person)
	(:Location)
	(l:Location)
	(x:Residence)
	(x:Location:Residence)

	Here we see nodes with variables and also anonymous nodes without variables. A node can be retrieved using one or more Labels.

	In the Movie database you will be working with in this course, the nodes in this graph are 
	labeled Movie or Person to represent two types of nodes.

COMMENTS IN CYPHER:

	In Cypher, you can place a comment (starts with //) anywhere in your Cypher to specify that 
	the rest of the line is interpreted as a comment.

	// anonymous node not be referenced later in the query
	()
	// variable p, a reference to a node used later
	(p)
	// anonymous node of type Person
	(:Person)
	// p, a reference to a node of type Person
	(p:Person)
	// p, a reference to a node of types Actor and Director
	(p:Actor:Director)	

EXAMINING THE DATA MODEL:

	When you are first learning about the data (nodes, labels, etc.) in a graph, it is helpful 
	to examine the data model of the graph. You do so by executing CALL db.schema.visualization(), which calls the Neo4j procedure that returns information about the nodes, labels, and relationships in the graph.

	For example, when we run this procedure in our training environment, we see the following in 
	the result pane. Here we see that the graph has 2 labels defined for nodes, Person and Movie. 
	Each type of node is displayed in a different color. The relationships between nodes are also displayed, which you will learn about later in this module.
	
SYNTAX: USING MATCH AND RETURN:

	The most widely used Cypher clause is MATCH. The MATCH clause performs a pattern match 
	against the data in the graph. During the query processing, the graph engine traverses 
	the graph to find all nodes that match the graph pattern. As part of query, you can return 
	nodes or data from the nodes using the RETURN clause. The RETURN clause must be the last clause 
	of a query to the graph. In the course, Creating Nodes and Relationships in Neo4j 4.x, you will learn how to use MATCH to select nodes and data for updating the graph. First, you will learn
	how to simply return nodes.

	Syntax examples for a query:

	Syntax
	MATCH (variable)
	RETURN variable
	Syntax
	MATCH (variable:Label)
	RETURN variable
	Notice that the Cypher keywords MATCH and RETURN are upper-case. This coding convention is described in the Cypher Style Guide and will be used in this training. This MATCH clause 
	returns all nodes in the graph, where the optional Label is used to return a subgraph if
	the graph contains nodes of different types. The variable must be specified here, otherwise 
	the query will have nothing to return.	

	Example: MATCH and RETURN
		Retrieve all nodes:

		Cypher
		Copy to Clipboard
		MATCH (n) 			// returns all nodes in the graph
		RETURN n
		
		Example: Retrieve all Person nodes
		Cypher
		Copy to Clipboard
		MATCH (p:Person) 	// returns all Person nodes in the graph
		RETURN p
		
		When we execute the Cypher statement, MATCH (p:Person) RETURN p, the graph engine 
		returns all nodes with the label Person. The default view of the returned nodes are 
		the nodes that were referenced by the variable p.

		A NOTE ?:
			When you specify a pattern for a MATCH clause, you should always specify a node 
			label if possible. In doing so, the graph engine uses an index to retrieve the 
			nodes which will perform better than not using a label for the MATCH.

		One thing to notice in this example is that some of the displayed nodes are connected 
		by the FOLLOWS relationship. The visualization shows the relationship between these 
		nodes because we have specified Connect result nodes in our Neo4j Browser settings. 
		Some of the Person nodes represent people who reviewed Movies and as such, they follow 
		each other.

VIEWING NODES AS TABLE DATA:
	We can also view the nodes as table data where the nodes and their associated property 
	values are shown in a JSON-style format.

	When nodes are displayed as table values, the node labels and ids are also shown if you 
	are using version 4.1 or later. Node ids are unique identifiers and are set by the graph 
	engine when a node is created.

PROPERTIES:

	In Neo4j, a node (and a relationship, which you will learn about later) can have properties
	that are used to further define a node. A property is identified by its property key. Recall 
	that nodes are used to represent the entities of your business model. A property is defined 
	for a node and not for a type of node. All nodes of the same type need not have the same properties.

	For example, in the Movie graph, all Movie nodes have both title and released properties. 
	However, it is not a requirement that every Movie node has a property, tagline.

	Properties can be used to filter queries so that a subset of the graph is retrieved. In addition, with the RETURN clause, you can return property values from the retrieved nodes, rather than the nodes.

	Examining property keys:

		As you prepare to create Cypher queries that use property values to filter a query, 
		you can view the values for property keys of a graph by simply clicking the Database 
		icon in Neo4j Browser. Alternatively, you can execute CALL db.propertyKeys(), which 
		calls the Neo4j library method that returns the property keys for the graph.

		Here is what you will see in the result pane when you call the method to return the 
		property keys in the Movie graph. This result stream contains all property keys in the 
		graph. It does not display which nodes utilize these property keys.


SYNTAX: RETRIEVING NODES FILTERED BY A PROPERTY VALUE
	You have learned previously that you can filter node retrieval by specifying a label. 
	Another way you can filter a retrieval is to specify a value for a property. Any node 
	that matches the value will be retrieved.

	Here are simplified syntax examples for a query where we specify one or more values for 
	properties that will be used to filter the query results and return a subset of the graph:

	Syntax
		MATCH (variable {propertyKey: propertyValue})
		RETURN variable
	Syntax
		MATCH (variable:Label {propertyKey: propertyValue})
		RETURN variable
	Syntax
		MATCH (variable {propertyKey1: propertyValue1, propertyKey2: propertyValue2})
		RETURN variable
	Syntax
		MATCH (variable:Label {propertyKey: propertyValue, propertyKey2: propertyValue2})
		RETURN variable


	BEAUTY EXAMPLE:

		Filtering query by year born:

			MATCH ( p:Person {born:1970} ) 
			RETURN p ;

		Filtering using two properties:
			MATCH (m:Movie {released: 2003, tagline: 'Free your mind'}) 
			RETURN m;

		NOTE: ALL PROPERTIES HAVE TO DO MATCH TO SOLVE A RETURN SET OF GRAPH ELEMENTS:
			ALL PROPERTIES HAVE TO DO MATCH, REMEMBER MICHEL.


	IN NEO4J YOU CAN RETURN THE NODE PROPERTIES:

		EXAMPLE:

			MATCH (p:Person {born: 1965}) RETURN p.name, p.born;

		THE PROPERTIES CAN BE DIFFERENT TO THE FILTER PROPERTIES.

		INTERESTING OBSERVATION: IF THE RETURN OBJECT.PROPERTY is NOT defines to the node
		TYPE, THE ANSWER IS null, NULL. (TRY remember this.)


SYNTAX: SPECIFYING ALIASES FOR COLUMN HEADINGS:
	
	If I want to customize the headers for a table containing property values I can use an
	ALIAS in the column header.

	MATCH (variable:Label {propertyKey1: propertyValue1})
	RETURN variable.propertyKey2 AS alias2

	A BEAUTY EXAMPLE:
		MATCH (p:Person {born: 1965})
		RETURN p.name AS name, p.born AS `birth year`


RELATIONSHIPS:
	Relationships are what make Neo4j graphs such a powerful tool for connecting complex 
	and deep data. A relationship is a directed connection between two nodes that has a 
	relationship type (name). In addition, a relationship can have properties, just like 
	nodes. In a graph where you want to retrieve nodes, you can use relationships between 
	nodes to filter a query.

	TO REMEMBER IN NEO4J RELATIONS ARE THE KEY: BE CAREFULL COWBOY.


A LEGEND NAMED : ASCII art
	Thus far, you have learned how to specify a node in a MATCH clause. You can specify 
	nodes and their relationships to traverse the graph and quickly find the data of 
	interest.

	Here is how Cypher uses ASCII art to specify the path used for a query:

		THIS CAN BE DECISIVE Michel and to remember 3:) :

			Syntax
			()          // a node
			()--()      // 2 nodes have some type of relationship
			()-[]-()    // 2 nodes have some type of relationship
			()-->()     // the first node has a relationship to the second node
			()<--()     // the second node has a relationship to the first node


SYNTAX: QUERYING USING RELATIONSHIPS:
	In your MATCH clause, you specify how you want a relationship to be used to perform 
	the query. The relationship can be specified with or without direction.

	Here are simplified syntax examples for retrieving a set of nodes that satisfy one 
	or more directed and typed relationships:

	Syntax
		MATCH (node1)-[:REL_TYPE]->(node2)
		RETURN node1, node2
	Syntax
		MATCH (node1)-[:REL_TYPEA | REL_TYPEB]->(node2)
		RETURN node1, node2

	where:

	node1           is a specification of a node where you may include node labels and property 
	                values for filtering.

	REL_TYPE        is the type (name) for the relationship. For this syntax the relationship 
					is from node1 to node2.

	REL_TYPEA ,     are the relationships from node1 to node2. The nodes are returned if at 
	REL_TYPEB       least one of the relationships exists.

	node2           is a specification of a node where you may include node labels and property 
	                values for filtering.


EXAMINING RELATIONSHIPS:
	
	Here is importan one of my favorite neo4j commands:

		CALL db.schema.visualization();

	Using a relationship in a query(Beauty example):

		MATCH (p:Person)-[rel:ACTED_IN]->(m:Movie {title: 'The Matrix'})
		RETURN p, rel, m;

	Specify labels whenever possible
		Important: You specify node labels whenever possible in your queries as it 
		optimizes the retrieval in the graph engine. That is, you should not specify 
		the previous query as:

		Cypher
			MATCH (p)-[rel:ACTED_IN]->(m {title:'The Matrix'})
			RETURN p,m

		IF I DON'T SPECIFY LABELS MY CYPHER WILL BE SLOW, IMPORTANT USE LABELS WHENEVER
		POSSIBLE.

	Querying by multiple relationships
		Here is another example where we want to know the movies that Tom Hanks acted 
		in or directed:

		Cypher
			MATCH (p:Person {name: 'Tom Hanks'})-[:ACTED_IN|DIRECTED]->(m:Movie)
			RETURN p.name, m.title

		At this point I was testing this:
			MATCH (p:Person {name: 'Tom Hanks'})-[act:ACTED_IN|DIRECTED]->(m:Movie)
			RETURN p, act, m

		An observation was : If I use properties neo4j browser does not draw the graph.
		If I use properties neo4j browser return only a table.


USING ANONYMOUS NODES IN A QUERY:

	Suppose you wanted to retrieve the actors that acted in The Matrix, but you do not 
	need any information returned about the Movie node. You need not specify a variable 
	for a node in a query if that node is not returned or used for later processing in 
	the query. You can simply use the anonymous node in the query as follows:

	Cypher:
		// Interesting lesson about take only the data you need.
		MATCH (p:Person)-[:ACTED_IN]->(:Movie {title: 'The Matrix'})
		RETURN p.name


USING AN ANONYMOUS RELATIONSHIP FOR A QUERY:
	
	Suppose you want to find all people who are in any way connected to the movie, 
	The Matrix. You can specify an empty relationship type in the query so that 
	all relationships are traversed and the appropriate results are returned. 
	In this example, we want to retrieve all Person nodes that have any type of 
	connection to the Movie node, with the title, The Matrix. This query returns 
	more nodes with the relationships types, DIRECTED, ACTED_IN, and PRODUCED.

	Cypher:
		// Interesting lesson about use anonymous relationships to have the data
		// you need.
		MATCH (p:Person)-->(m:Movie {title: 'The Matrix'})
		RETURN p, m;

	MORE ANONYMOUS RELATIONSHIPS:
	Here are other examples of using the anonymous relationship:

	Cypher:
		MATCH (p:Person)--(m:Movie {title: 'The Matrix'})
		RETURN p, m
	Cypher:
		MATCH (p:Person)-[]-(m:Movie {title: 'The Matrix'})
		RETURN p, m
	Cypher:
		MATCH (m:Movie)<--(p:Person {name: 'Keanu Reeves'})
		RETURN p, m


RETRIEVING THE RELATIONSHIP TYPES:
	
	There is a built-in function, type() that returns the type of a relationship.

	Here is an example where we use the rel variable to hold the relationships retrieved. 
	We then use this variable to return the relationship types.

	Cypher
		MATCH (p:Person)-[rel]->(:Movie {title:'That Thing You Do'})
		RETURN p.name, type(rel);

		// Important Note ; when I was running this CypherQL I thought why not try 
		// execute a group , and searching I found something about collect, with the 
		// search I found some interesting things and I was testing the next Query:
		MATCH (p:Person)-[rel]->(m:Movie {title:'That Thing You Do'})
		RETURN p.name AS `Person`,  collect(type(rel)) AS `Participation`, m.title AS `Movie`
		ORDER BY p.name;

PROPERTIES FOR RELATIONSHIPS:
	
	Recall that a node can have as set of properties, each identified by its property key. Relationships can also have properties. This enables your graph model to provide more 
	data about the relationships between the nodes.

	Here is an example from the Movie graph. The movie, The Da Vinci Code has two people 
	that reviewed it, Jessica Thompson and James Thompson. Each of these Person nodes has 
	the REVIEWED relationship to the Movie node for The Da Vinci Code. Each relationship 
	has properties that further describe the relationship using the summary and rating 
	properties.

	MATCH (p:Person)-[:REVIEWED {rating: 65}]->(:Movie {title: 'The Da Vinci Code'})
	RETURN p.name;

	// After test this query I thought : Neo4j is really powerfull, cypher remove a Join
	// and delete complexity , it is Art.

PATTERNS IN THE GRAPH:
	Thus far, you have learned how to specify nodes, properties, and relationships in 
	your Cypher queries. Since relationships are directional, it is important to understand 
	how patterns are used in graph traversal during query execution. How a graph is traversed 
	for a query depends on what directions are defined for relationships and how the pattern 
	is specified in the MATCH clause.

	Here is an example of where the FOLLOWS relationship is used in the Movie graph. Notice 
	that this relationship is directional.

	Cypher :
		MATCH  (p:Person)-[:FOLLOWS]->(:Person {name:'Angela Scope'})
		RETURN p

		For this query the Person node for Angela Scope is the anchor of the query. It is 
		the first node that is retrieved from the graph. Then the query engine looks for 
		all relationships into this node and retrieves them. In this case there is only one 
		relationship that is defined that points to the Angela Scope node, Paul Blythe.

		REVERSING THE TRAVERSAL IMPORTANT NOTION:
			If we reverse the direction in the pattern, the query returns different results:

			// IN THIS QUERY YOU CAN FEEL AN AGRESSIVE CYPHERQL , REVIEW THE PREVIOS CypherQuery
			MATCH  (p:Person)<-[:FOLLOWS]-(:Person {name:'Angela Scope'})
			RETURN p

		QUERYING A RELATIONSHIP IN BOTH DIRECTIONS:
			
			We can also find out what Person nodes are connected by the FOLLOWS relationship 
			in either direction by removing the directional arrow from the pattern.

			Cypher:
			MATCH  (p1:Person)-[:FOLLOWS]-(p2:Person {name:'Angela Scope'})
			RETURN p1, p2

			// About this query using -[]- I reviewed and found the function id();
			// In the process I wrotte the query ;

			MATCH  (p1:Person)-[rel:FOLLOWS]-(p2:Person {name:'Angela Scope'})
			RETURN p1.name, id(p1), rel, p2.name, id(p2);

			// If you review the table you can find the rel with the start and finish
			// locations using identity of the nodes, it is very interesting.

		TRAVERSING MULTIPLE RELATIONSHIPS:
		
			Since we have a graph, we can traverse through nodes to obtain relationships 
			further into the traversal.

			For example, we can write a Cypher query to return all followers of the 
			followers of Jessica Thompson.

			// Cypher with pure beauty SUREEE.
			MATCH  (p:Person)-[:FOLLOWS]->(:Person)-[:FOLLOWS]->(:Person {name:'Jessica Thompson'})
			RETURN p;

		VARIATION ON THE TRAVERSAL:
			
			This query could also be modified to return each person along the matched 
			path by specifying variables for the nodes and returning them. For example:

				Cypher
				MATCH  (p:Person)-[:FOLLOWS]->(p2:Person)-[:FOLLOWS]->(p3:Person {name:'Jessica Thompson'})
				RETURN p.name, p2.name, p3.name

			For this query, although the query engine traverses the path from Jessica Thompson 
			to James Thompson, it finds that the James Thompson node does not match the entire 
			path specified.

		USING PATTERNS TO FOCUS THE QUERY:

			As you gain more experience with Cypher, you will see how patterns in your queries 
			enable you to focus on the relationships in the graph. For example, suppose we want 
			to retrieve all unique relationships between an actor, a movie, and a director. This 
			query will return many unique rows of information that provide this pattern in the graph:

			// aBOUT THIS QUERY I Thought : how remove repeated ?
			MATCH (a:Person)-[:ACTED_IN]->(m:Movie)<-[:DIRECTED]-(d:Person)
			RETURN a.name, m.title, d.name

			// then I wrote the query:
			MATCH 
				(a:Person)-[:ACTED_IN]->(m:Movie{title: 'The Matrix'})<-[:DIRECTED]-(d:Person)
			RETURN 
			    a.name AS `Person:`, m.title AS `In Movie`, collect(d.name) AS `Directed by:`
			;

		PATHS, AN INTERESTING CONCEPT IN NEO4J:

			RETURNING PATHS:

				In addition, you can assign a variable to the path and return the path as 
				follows:

				MATCH  path = (:Person)-[:FOLLOWS]->(:Person)-[:FOLLOWS]->(:Person {name:'Jessica Thompson'})
				RETURN  path

				HERE I VIEW: I CAN UNDERSTAND THE PATH IN THE NEO4J GRAPHICALL WAY
				BUT IN THE JSON IS ANOTHER THING.

				in the json A PATH HAS ONLY ONE RECORD IMPORTANT OBSERVATION:

			RETURNING MULTIPLE PATHS:
				
				Here is another example where multiple paths are returned. The query is to return 
				all paths from actors to a movie that was directed by Ron Howard.

				MATCH  path = (:Person)-[:ACTED_IN]->(:Movie)<-[:DIRECTED]-(:Person {name:'Ron Howard'})
				RETURN  path
				
				Multiple paths are returned. Even if we set Neo4j Browser to not connect result 
				nodes, the nodes are shown as connected in the visualization because we are 
				returning paths, not nodes:
				
				MY IDEA ABOUT PATHS CHANGED HERE: I HAD TO REVIEW THE GRAPH IN NEO4J BROWSER
				TO UNDERSTAND.

				----------------------------------------------------------------------------
				A best practice is to specify direction in your MATCH statements. This will 
				optimize queries, especially for larger graphs.
				----------------------------------------------------------------------------

	
CYPHER STYLE RECOMMENDATIONS:

	Here are the Neo4j-recommended Cypher coding standards that we use in this training:

	Node labels are CamelCase and begin with an upper-case letter 
	(examples: Person, NetworkAddress). Note that node labels are 
	case-sensitive.

	Property keys, variables, parameters, aliases, and functions 
	are camelCase and begin with a lower-case letter (examples: 
	businessAddress, title). Note that these elements are case-sensitive.

	Relationship types are in upper-case and can use the underscore. 
	(examples: ACTED_IN, FOLLOWS). Note that relationship types are 
	case-sensitive and that you cannot use the "-" character in a 
	relationship type.

	Cypher keywords are upper-case (examples: MATCH, RETURN). Note that 
	Cypher keywords are case-insensitive, but a best practice is to use 
	upper-case.

	String constants are in single quotes, unless the string contains a 
	quote or apostrophe (examples: 'The Matrix', "Something’s Gotta Give"). 
	Note that you can also escape single or double quotes within strings 
	that are quoted with the same using a backslash character.

	Specify variables only when needed for use later in the Cypher statement.

	Place named nodes and relationships (that use variables) before anonymous 
	nodes and relationships in your MATCH clauses when possible.

	Specify anonymous relationships with -->, --, or <--.

	Example: Using style recommendations

	Here is an example showing some best coding practices:

		CypheR:

		MATCH (:Person {name: 'Diane Keaton'})-[movRel:ACTED_IN]->
		(:Movie {title:"Something's Gotta Give"})
		RETURN movRel.roles

		// fROM THIS QUERY I LEARNED : ONLY A RELATION IS NOT DRAWED BY NEO4J
		// IN THE BROWSER.

	this SOURCE IS VERY IMPORTANT:

		https://neo4j.com/docs/cypher-manual/current/styleguide/


























