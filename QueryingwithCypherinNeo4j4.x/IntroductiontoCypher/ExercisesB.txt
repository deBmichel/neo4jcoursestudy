Exercise 1:
	Retrieve all Movie nodes that have a released property value of 2003.

	QUERY ANS:
		MATCH (m:Movie {released: 2003}) RETURN m;

Exercise 2:
	-> View the results you just viewed in Neo4j Browser as a table.

	I gave click in the table option of the Browser.

	I thought in use an Alias, but I reviewed the instructions and the idea was
	not the correct, only was about give click in the table option of the Browser.

	-> Retrieve all movie nodes in the database and view the data as a table. Notice 
	   the values for the released property for each node.

	Here I used the query:
		MATCH (m:Movie) return m;

	And I found some released values like:
		1999, 2003, 1997, 1992, 1986, 2000 and others.

	-> Try querying the graph using different years.

	Here I used queries like:
		MATCH (m:Movie {released: 1986}) RETURN m; (records = 2)
		MATCH (m:Movie {released: 1992}) RETURN m; (records = 4) 
		MATCH (m:Movie {released: 1997}) RETURN m; (records = 2)
		MATCH (m:Movie {released: 1999}) RETURN m; (records = 4)
		MATCH (m:Movie {released: 2000}) RETURN m; (records = 3)
		MATCH (m:Movie {released: 2003}) RETURN m; (records = 3)

		Additionally I tried using an inexistent year 3:) 

		MATCH (m:Movie {released: 666}) RETURN m; (records = 0) but the message was:
		(no changes, no records) it is important to review.

Exercise 3:
	You want to know the existing property keys used in a graph. This will help you to write Cypher queries that utilizes property keys for filtering data or for returning data.

	Query the database for all property keys.

	Here I used the query:
		CALL db.propertyKeys(); and I found 19 registers but without the nodes using the properties.
			--------------
			propertyKey
			--------------
			"isbn"
			"title"
			"price"
			"description"
			"createdAt"
			"rating"
			"text"
			"username"
			"placedAt"
			"orderID"
			"address"
			"location"
			"name"
			"color"
			"tagline"
			"released"
			"born"
			"roles"
			"summary"

		But searching in Google I found the next query in https://neo4j.com/developer/kb/how-do-i-display-the-nodes-with-the-most-properties/:

		MATCH (n)
		RETURN labels(n), keys(n), size(keys(n)), count(*)
		ORDER BY size(keys(n)) DESC;

		And the new return was:

		labels(n)	   keys(n)	                            size(keys(n))	count(*)
		------------------------------------------------------------------------
		["Movie"]	   ["tagline", "title", "released"]	    3	            16
		["Movie"]	   ["released", "title", "tagline"]	    3	            12
		["Movie"]	   ["title", "tagline", "released"]	    3	            8
		["Movie"]	   ["tagline", "released", "title"]	    3	            1
		["Person"]	   ["born", "name"]	                    2	            128
		["Movie"]	   ["title", "released"]	            2	            1
		["Person"]	   ["name"]	                            1	            5

		It was an interesting query, sure and I can imagine some new ideas :)

Exercise 4:
	Retrieve all Movies released in a specific year, returning their titles (Instructions):
		-> Rather than returning the nodes that satisfy a query, you want to return data 
		from the nodes.

		-> Retrieve all Movies released in 2006, returning their titles.

	Here I used the query:
		MATCH (m:Movie {released:2006}) RETURN m.title AS `Movie Title`;

	(Taking it further - optional)

	-> Retrieve all Movie nodes and view them as a table. Observe the properties that Movie 
	nodes have.

	Here I had the result of the properties that I had from search in google.

	But I though in try neo4j and I used this:
		MATCH (m:Movie)
		RETURN labels(m), keys(m), size(keys(m)), count(*)
		ORDER BY size(keys(n)) DESC;

	The return was:
		labels(m)	keys(m)	                            size(keys(m))	count(*)
		------------------------------------------------------------------------
		["Movie"]	["tagline", "title", "released"]	3	            16
		["Movie"]	["released", "title", "tagline"]	3	            12
		["Movie"]	["title", "tagline", "released"]	3	            8
		["Movie"]	["tagline", "released", "title"]	3	            1
		["Movie"]	["title", "released"]	            2	            1

	-> Query the database using a different year and also return more property values.

	Finally I used this:

		MATCH (m:Movie )
		WHERE released > 0
		RETURN
			m.title    AS `Title`,
			m.tagline  AS `Tagline`, 
			m.released AS `Released`;

	And the result was:
		Title	Tagline	Released
		"The Matrix"	"Welcome to the Real World"	1999
		"The Matrix Reloaded"	"Free your mind"	2003
		"The Matrix Revolutions"	"Everything that has a beginning has an end"	2003
		"The Devil's Advocate"	"Evil has its winning ways"	1997
		"A Few Good Men"	"In the heart of the nation's capital, in a courthouse of the U.S. government, one man will stop at nothing to keep his honor, and one will stop at nothing to find the truth."	1992
		"Top Gun"	"I feel the need, the need for speed."	1986
		"Jerry Maguire"	"The rest of his life begins now."	2000
		"Stand By Me"	"For some, it's the last real taste of innocence, and the first real taste of life. But for everyone, it's the time that memories are made of."	1986
		"As Good as It Gets"	"A comedy from the heart that goes for the throat."	1997
		"What Dreams May Come"	"After life there is more. The end is just the beginning."	1998
		"Snow Falling on Cedars"	"First loves last. Forever."	1999
		"You've Got Mail"	"At odds in life... in love on-line."	1998
		"Sleepless in Seattle"	"What if someone you never met, someone you never saw, someone you never knew was the only someone for you?"	1993
		"Joe Versus the Volcano"	"A story of love, lava and burning desire."	1990
		"When Harry Met Sally"	"Can two friends sleep together and still love each other in the morning?"	1998
		"That Thing You Do"	"In every life there comes a time when that thing you dream becomes that thing you do"	1996
		"The Replacements"	"Pain heals, Chicks dig scars... Glory lasts forever"	2000
		"RescueDawn"	"Based on the extraordinary true story of one man's fight for freedom"	2006
		"The Birdcage"	"Come as you are"	1996
		"Unforgiven"	"It's a hell of a thing, killing a man"	1992
		"Johnny Mnemonic"	"The hottest data on earth. In the coolest head in town"	1995
		"Cloud Atlas"	"Everything is connected"	2012
		"The Da Vinci Code"	"Break The Codes"	2006
		"V for Vendetta"	"Freedom! Forever!"	2006
		"Speed Racer"	"Speed has no limits"	2008
		"Ninja Assassin"	"Prepare to enter a secret world of assassins"	2009
		"The Green Mile"	"Walk a mile you'll never forget."	1999
		"Frost/Nixon"	"400 million people were waiting for the truth."	2008
		"Hoffa"	"He didn't want law. He wanted justice."	1992
		"Apollo 13"	"Houston, we have a problem."	1995
		"Twister"	"Don't Breathe. Don't Look Back."	1996
		"Cast Away"	"At the edge of the world, his journey begins."	2000
		"One Flew Over the Cuckoo's Nest"	"If he's crazy, what does that make you?"	1975
		"Something's Gotta Give"	null	2003
		"Bicentennial Man"	"One robot's 200 year journey to become an ordinary man."	1999
		"Charlie Wilson's War"	"A stiff drink. A little mascara. A lot of nerve. Who said they couldn't bring down the Soviet empire."	2007
		"The Polar Express"	"This Holiday Season... Believe"	2004
		"A League of Their Own"	"Once in a lifetime you get a chance to do something different."	1992

		And I reviewed the query:

		MATCH (m:Movie {tagline:null})
		RETURN
			m.title    AS `Title`,
			m.tagline  AS `Tagline`, 
			m.released AS `Released`;

		But I did not have the expected result, but if I use:

		MATCH (m:Movie)
		WHERE m.tagline is null
		RETURN
			m.title    AS `Title`,
			m.tagline  AS `Tagline`, 
			m.released AS `Released`;

		I obtained the expected result:
			Title	                    Tagline	  Released
			"Something's Gotta Give"	null	  2003

Exercise 5:
	-> Display title, released, and tagline values for every Movie node in the graph (Instructions)

		When you start working with a graph, it is sometimes helpful to simply view 
		property values. This can sometimes help to inform you about future queries 
		you may want to execute against the graph.

		-> Retrieve all Movie nodes from the database and return the title, released, and 
		tagline values.

		Here I used the query:
			MATCH (m:Movie)
			RETURN
				m.title    AS `Title`,
				m.tagline  AS `Tagline`, 
				m.released AS `Released`;

Exercise 6: 
	-> Display more user-friendly headers in the table (Instructions)

	Modify the query you just ran so that the headings for the columns of the table returned are 
	more descriptive.

		Here I used the query:
			MATCH (m:Movie)
			RETURN
				m.title    AS `Movie Title`,
				m.tagline  AS `Movie Tagline`, 
				m.released AS `Movie Released Year`;





