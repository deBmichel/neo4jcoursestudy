There are a number of numeric functions you can use in your conditions. See the Neo4j Cypher Manual’s section Mathematical Functions for more information.

A special condition in a query is when the retrieval returns an unknown value called null. You should read the Neo4j Cypher Manual’s section Working with null to understand how null values are used at runtime.

See the String functions section of the Neo4j Cypher Manual for more information. 
It is sometimes useful to use the built-in string functions to modify the data 
that is returned in the query in the RETURN clause.

In the course Creating Nodes and Relationships in Neo4j 4.x, you will learn how to create 
lists from your queries by aggregating data in the graph.

There are a number of syntax elements of Cypher that we have not covered in this training. 
For example, you can specify CASE logic in your conditional testing for your WHERE clauses. 
You can learn more about these syntax elements in the Neo4j Cypher Manual and the Cypher 
Refcard.

The APOC library is very useful if you want to query the graph to obtain subgraphs.

You can read more about working with lists in the List Functions section of the Neo4j 
Cypher Manual.

You can learn more about map projections in the Cypher Reference Manual.

Consult the Neo4j Cypher Manual for more information about the built-in functions 
available for working with data of all types.

What you must be careful of, however is that when you create the node, it is not a duplicate 
node. In addition, you must ensure that there is always only one :ACTED_IN relationship 
created between two nodes. In the course, Using Indexes and Query Best Practices in 
Neo4j 4.x, you will learn how automatically eliminate duplication of nodes and relationships.

consult the Neo4j Operations Manual for more details about creating and maintaining indexes.

Please see the Cypher Reference Manual for more on using full-text schema indexes.

You will only be able to see the performance benefits of indexes with large graphs which is beyond
the scope of this training.

The training course, Cypher Query Tuning in Neo4j 4.x goes into greater depth about Cypher best practices and query tuning.

Graph data modeling is taught in the the course Graph Data Modeling for Neo4j. Query 
tuning is taught in the course Advanced Cypher.
