Exercise 14.1: Create an index (Instructions):

	Create a single-property index on the born property of a Person node naming the index
	PersonBornIndex.

		//My solution
			CREATE INDEX PersonBornIndex FOR (p:Person) ON (p.born)

		//The solution
			CREATE INDEX PersonBornIndex FOR (p:Person) ON (p.born)

Exercise 14.2: View index information (Instructions)

	View the indexes defined for the graph.

	Do you see indexes that you did not create with the CREATE INDEX statement?

		ANS:
			YES I see indexes that I did not create with the CREATE INDEX statement

	//My solution
		SHOW INDEXES
		CALL db.indexes()

	//The solution

		CALL db.indexes()

		The other indexes have been created because you previously created a uniqueness constraint 
		and a node key constraint, both of which create indexes. You will only be able to see the
		performance benefits of indexes with large graphs which is beyond the scope of this training.

Exercise 14.3: Drop an index (Instructions)

	Drop the single-property index you just created for the born property of the Person nodes.

	//My solution
		DROP INDEX PersonBornIndex

	//The solution
		DROP INDEX PersonBornIndex

Exercise 14.4: Create a full-text schema index (Instructions)

	Create a full-text schema index for the tagline property of the Movie nodes named 
	MovieTaglineFTIndex.

	//My solution
		CALL db.index.fulltext.createNodeIndex(
      		'MovieTaglineFTIndex',['Movie'], ['tagline']
      	)
			
	//The solution
		CALL db.index.fulltext.createNodeIndex(
			'MovieTaglineFTIndex',['Movie'], ['tagline']
		)

Exercise 14.5: View index information (Instructions)

	View the indexes defined for the graph.

		//My solution
			SHOW INDEXES
			CALL db.indexes()

		//The solution
			CALL db.indexes()

Exercise 14.6: Perform a query that uses the full-text schema index (Instructions)

	Write and execute a query to find all movies with taglines that contain the strings "real" 
	or "world".

	Write and execute a query to find all movies with taglines that contain the strings "real" 
	and "world".

	//My solution
		CALL db.index.fulltext.queryNodes(
			'MovieTaglineFTIndex', 'real') YIELD node, score
		RETURN node.title, score

		CALL db.index.fulltext.queryNodes(
			'MovieTaglineFTIndex', 'world') YIELD node, score
		RETURN node.title, score
		
		
		CALL db.index.fulltext.queryNodes(
			'MovieTaglineFTIndex', 'real world') YIELD node, score
		RETURN node.title, score
		
	//The solution
		CALL db.index.fulltext.queryNodes('MovieTaglineFTIndex', 'real OR world') YIELD node
		RETURN node.title, node.tagline

		CALL db.index.fulltext.queryNodes('MovieTaglineFTIndex', 'real AND world') YIELD node
		RETURN node.title, node.tagline

Exercise 14.6: Perform a query that uses the full-text schema index (Taking it further - optional)

	Perform more variations of full-text searches.

	Add score to the YIELD clause and return it to see what score the search produces.

		//My solution
			CALL db.index.fulltext.queryNodes('MovieTaglineFTIndex', 'real OR world') 
			YIELD node, score
			RETURN node.title, node.tagline, score

			CALL db.index.fulltext.queryNodes('MovieTaglineFTIndex', 'real AND world') 
			YIELD node, score
			RETURN node.title, node.tagline, score

Exercise 14.7: Drop the full-text schema index (Instructions)

	Drop the full-text schema index you just created.	

		//My solution
			CALL db.index.fulltext.drop('MovieTaglineFTIndex');
			SHOW INDEXES

		//The solution
			CALL db.index.fulltext.drop('MovieTaglineFTIndex')








