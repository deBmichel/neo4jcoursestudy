UNIQUENESS AND EXISTENCE IN THE GRAPH:

	You have seen that it is possible to create duplicate nodes in the graph. In most 
	graphs, you will want to prevent duplication of data. Unfortunately, you cannot 
	prevent duplication by checking the existence of the exact node (with properties) 
	as this type of test is not cluster or multi-thread safe as no locks are used. This 
	is one reason why MERGE is preferred over CREATE, because MERGE does use locks.

	In addition, you have learned that a node or relationship need not have a particular 
	property. What if you want to ensure that all nodes or relationships of a specific 
	type (label) must set values for certain properties?

	A third scenario with graph data is where you want to ensure that a set of property 
	values for nodes of the same type, have a unique value. This is the same thing as a 
	primary key in a relational database.

	All of these scenarios are common in many graphs.

	IN NEO4J, YOU CAN USE CYPHER TO:

		Add a uniqueness constraint that ensures that a value for a property is unique for 
		all nodes of that type.

		Add an existence constraint that ensures that when a node or relationship is created 
		or modified, it must have certain properties set.

		Add a node key that ensures that a set of values for properties of a node of a given 
		type is unique.

		Constraints and node keys that enforce uniqueness are related to indexes which you will 
		learn about later in this course.

		Existence constraints and node keys are only available in Enterprise Edition of Neo4j.

ENSURING THAT A PROPERTY VALUE FOR A NODE IS UNIQUE:

	You add a uniqueness constraint to the graph by creating a constraint that asserts that 
	a particular node property is unique in the graph for a particular type of node.

	Here is an example for ensuring that the title for a node of type Movie is unique:

	CREATE CONSTRAINT UniqueMovieTitleConstraint ON (m:Movie) ASSERT m.title IS UNIQUE

	Although the name of the constraint, UniqueMovieTitleConstraint is optional, Neo4j recommends
	that you name it. Otherwise, it will be given an auto-generated name. This Cypher statement 
	will fail if the graph already has multiple Movie nodes with the same value for the title 
	property. Note that you can create a uniqueness constraint, even if some Movie nodes do not 
	have a title property.

	If I try this after add the constraint the error is:

	try = CREATE (:Movie {title: 'The Matrix'})
	error = Neo.ClientError.Schema.ConstraintValidationFailed
			Node(0) already exists with label `Movie` and property `title` = 'The Matrix'

	EXAMPLE: UNIQUENESS AT RUNTIME:

		In addition, if you attempt to modify the value of a property where the uniqueness 
		assertion fails, the property will not be updated.

		MATCH (m:Movie {title: 'The Matrix'})
		SET m.title = 'Top Gun'

		Here is the result of running this Cypher statement on the Movie graph:

		Neo.ClientError.Schema.ConstraintValidationFailed
		Node(29) already exists with label `Movie` and property `title` = 'Top Gun'

ENSURING THAT PROPERTIES EXIST:

	Having uniqueness for a property value is only useful in the graph if the property 
	exists. In most cases, you will want your graph to also enforce the existence of 
	properties, not only for those node properties that require uniqueness, but for 
	other nodes and relationships where you require a property to be set. Uniqueness 
	constraints can only be created for nodes, but existence constraints can be created 
	for node or relationship properties.

	You add an existence constraint to the graph by creating a constraint that asserts 
	that a particular type of node or relationship property must exist in the graph when 
	a node or relationship of that type is created or updated.

	Recall that in the Movie graph, the movie, Something’s Gotta Give has no tagline property:

	MATCH (m:Movie)
	WHERE m.title CONTAINS 'Gotta'
	RETURN m	

	EXAMPLE: EXISTENCE CONSTRAINT ADDITION FAILS:

		Here is an example for adding the existence constraint to the tagline property of 
		all Movie nodes in the graph:

		CREATE CONSTRAINT ExistsMovieTagline ON (m:Movie) ASSERT exists(m.tagline)
		
		Here is the result of running this Cypher statement:

		Neo.DatabaseError.Schema.ConstraintCreationFailed
		Unable to create Constraint( type='NODE PROPERTY EXISTENCE', schema=(:Movie {tagline}) ):
		Node(154) with label `Movie` must have the property `tagline`

	EXAMPLE: ADDING THE EXISTENCE CONSTRAINT:

		We know that in the Movie graph, all :REVIEWED relationships currently have a 
		property, rating. We can create an existence constraint on that property as follows:

			CREATE CONSTRAINT ExistsREVIEWEDRating
			ON ()-[rel:REVIEWED]-() ASSERT exists(rel.rating)

		Notice that when you create the constraint on a relationship, you need not specify the
		direction of the relationship. With the result:

		OUTPUT:

			Added 1 constraint, completed after 13 ms.

	ATTEMPTING TO ADD RELATIONSHIP WITHOUT PROPERTY:

		So after creating this constraint, if we attempt to create a :REVIEWED relationship 
		without setting the rating property:

		MATCH (p:Person), (m:Movie)
		WHERE p.name = 'Jessica Thompson' AND
			  m.title = 'The Matrix'
		MERGE (p)-[:REVIEWED {summary: 'Great movie!'}]->(m)

		We see this error:

		Neo.ClientError.Schema.ConstraintValidationFailed
		Relationship(268) with type `REVIEWED` must have the property `rating`

	ATTEMPTING TO REMOVE PROPERTY FROM RELATIONSHIP:

		You will also see this error if you attempt to remove a property from a node or 
		relationship where the existence constraint has been created in the graph.

		MATCH (p:Person)-[rel:REVIEWED]-(m:Movie)
		WHERE p.name = 'Jessica Thompson'
		REMOVE rel.rating

		Here is the result:

			Neo.ClientError.Schema.ConstraintValidationFailed
			Relationship(244) with type `REVIEWED` must have the property `rating`

	RETRIEVING CONSTRAINTS DEFINED FOR THE GRAPH:
		
		You can query for the set of constraints defined in the graph as follows:

		CALL db.constraints() // HERE IN MY AURA VERSION I FOUND THE DEPRECATION WARNING AND THE USE OF

		SHOW CONSTRAINTS // THE CORRECT COMMAND

		In Neo4j 4.2 and later you can use SHOW CONSTRAINTS.
		
		And here is what is returned from the graph:

		is interesting the difference :

			CALL db.constraints() produce :

				name	description	details
"ExistsREVIEWEDRating"	"CONSTRAINT ON ()-[ reviewed:REVIEWED ]-() ASSERT (reviewed.rating) IS NOT NULL"	"Constraint( id=4, name='ExistsREVIEWEDRating', type='RELATIONSHIP PROPERTY EXISTENCE', schema=-[:REVIEWED {rating}]- )"
"UniqueMovieTitleConstraint"	"CONSTRAINT ON ( movie:Movie ) ASSERT (movie.title) IS UNIQUE"	"Constraint( id=3, name='UniqueMovieTitleConstraint', type='UNIQUENESS', schema=(:Movie {title}), ownedIndex=2 )"


			SHOW CONSTRAINTS PRODUCE:

			id	name	type	entityType	labelsOrTypes	properties	ownedIndexId
4	"ExistsREVIEWEDRating"	"RELATIONSHIP_PROPERTY_EXISTENCE"	"RELATIONSHIP"	["REVIEWED"]	["rating"]	null
3	"UniqueMovieTitleConstraint"	"UNIQUENESS"	"NODE"	["Movie"]	["title"]	2

		iS MORE INTERESTING CALL db.constraints() FOR ME.

DROPPING CONSTRAINTS:

	You remove constraints defined for the graph with the DROP CONSTRAINT clause.

	Here we drop the existence constraint named ExistsREVIEWEDRating:

	DROP CONSTRAINT ExistsREVIEWEDRating
	
	With the result:

	Removed 1 constraint, completed after 10 ms.

CREATING MULTI-PROPERTY UNIQUENESS/EXISTENCE CONSTRAINT: NODE KEY:

	A node key is used to define the uniqueness and existence constraint for multiple 
	properties of a node of a certain type. A node key is also used as a composite index 
	in the graph.

	Suppose that in our Movie graph, we will not allow a Person node to be created where 
	both the name and born properties are the same. We can create a constraint that will 
	be a node key to ensure that this uniqueness for the set of properties is asserted.

	Here is an example to create this node key:

		CREATE CONSTRAINT UniqueNameBornConstraint
		ON (p:Person) ASSERT (p.name, p.born) IS NODE KEY
	
	Here is the result of running this Cypher statement on our Movie graph:

		Neo.DatabaseError.Schema.ConstraintCreationFailed
		Unable to create Constraint( type='NODE PROPERTY EXISTENCE', schema=(:Person {name, born}) ):
		Node(129) with label `Person` must have the properties (name, born)

	This attempt to create the constraint failed because there are Person nodes in the graph 
	that do not have the born property defined.

CLEANING UP THE GRAPH TO SUPPORT CONSTRAINT
	
	If we set these properties for all nodes in the graph that do not have born properties with:

	MATCH (p:Person)
	WHERE NOT exists(p.born)
	SET p.born = 0

	Then the creation of the node key succeeds:

	BUT :

		Neo.DatabaseError.Schema.ConstraintCreationFailed
		Unable to create Constraint( name='UniqueNameBornConstraint', type='NODE KEY', schema=(:Person {name, born}) ):
		Both Node(185) and Node(189) have the label `Person` and properties `name` = 'Gary Sinise', `born` = 0

	now is needed delete duplicated data, Node(185) and Node(189) are equals.

		MATCH (p:Person)
		WHERE id(p) = 185
		DETACH DELETE p

	AND again :

		CREATE CONSTRAINT UniqueNameBornConstraint
		ON (p:Person) ASSERT (p.name, p.born) IS NODE KEY

	NOW HAS THE OUTPUT:

		Added 1 constraint, completed after 46 ms.

	Any subsequent attempt to create or modify an existing Person node with name or born values 
	that violate the uniqueness constraint as a node key will fail.

	TESTING THE NODE KEY - DUPLICATE DATA:

		For example, executing this Cypher statement will fail:

			CREATE (:Person {name: 'Jessica Thompson', born: 0})
		
		Here is the result:

			Neo.ClientError.Schema.ConstraintValidationFailed
			Node(169) already exists with label `Person` and properties `name` = 'Jessica Thompson', `born` = 0


		If you have defined a node key in the graph that, for example, represents the data in 
		two properties, every node must contain a unique value for the properties. Additionally, 
		every node must contain the properties of the node key.

	TESTING THE NODE KEY - REMOVING DATA:
		
		If you attempt to remove a property that is used for a node key:

		MATCH (p:Person {name: 'Jessica Thompson', born: 0})
		REMOVE p.born

		You will see this error:

		Neo.ClientError.Schema.ConstraintValidationFailed
		Node(169) with label `Person` must have the properties (name, born)



















